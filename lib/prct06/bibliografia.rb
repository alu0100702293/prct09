module Prct06

	class Bibliografia
		include Enumerable
		attr_reader  :head, :tail
		
		Nodo = Struct.new :value, :next, :prev

		def initialize (referencias)
			if referencias.is_a? (Referencia||NILL)
				@head = Nodo.new(referencias, nil, nil)
				@tail = nil
			elsif referencias.nil?
				@head = nil
				@tail = nil
			else raise "Debe crearse una clase Referencia y pasarla como parámetro al constructor de esta clase" end
		end
		def each
			return nil if @head.nil?
			node = @head
			until node.nil?
				yield node.value
				node = node.next
			end
		end
		def add (value)
			if value.is_a? Referencia #Si se pasa una sola referencia
				if @head == nil
					@head = Nodo.new(value, nil, nil)
					@tail = nil
				else
					if @tail == nil
						@tail = Nodo.new(value, nil, @head)
						@head.next=@tail
					else
						aux = @tail
						@tail = Nodo.new(value, nil)
						aux.next = @tail
						tail.prev = aux
					end
				end
			elsif value.is_a? Array #Si se pasa un array de referencias
				value.each do |i|
					self.add(i)
					
				end
			else raise "Se debe pasar una Referencia o un Array de Referencias"
			end
		
		end
		
		def popup
			aux = self.head
			@head = aux.next
			return aux
		end
		
	end
end