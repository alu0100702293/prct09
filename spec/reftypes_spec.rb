require "prct06"

describe Prct06::Referencia do
    describe Prct06::Libro do
        
        before :each do
          @libro = Prct06::Libro.new(["Dave Thomas","Andy Hunt", "Chad Fowler"], 
										"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide",
										nil, "Pragmatic Bookshelf", "4 edition", "July 7, 2013", 
										["ISBN-13: 978-1937785499","ISBN-10: 1937785491"])
        end
        it "El objeto libro es una instancia de Libro" do
            @libro.instance_of? Prct06::Libro
        end
        it "Libro es hijo de Referencia" do
            @libro.is_a?Prct06::Referencia
            expect(@libro.class.superclass).to eq (Prct06::Referencia)
        end
    end
    describe Prct06::Articulo do
        before :each do
          @revista = Prct06::Articulo.new("Jacobson Martin","Isolation,Indentification and Synthesis of the Sex Attractant of Gypsy Moth","Oct. 14, 1960","first edition","vol. 132","n. 3433","Sciense", 1)
        end
        
        it "El objeto libro es una instancia de Libro" do
            @revista.instance_of? Prct06::Articulo
        end
        it "Artículo es hijo de Referencia"do
            @revista.is_a?Prct06::Referencia
            expect(@revista.class.superclass).to eq (Prct06::Referencia)
        end
    end
end